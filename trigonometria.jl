# MAC0110 - MiniEP7
# João Vitor Magalhães Leite - 11849414
function bernoulli(n)
	n *= 2
	A = Vector{Rational{BigInt}}(undef, n + 1)
	for m = 0 : n
		A[m + 1] = 1 // (m + 1)
		for j = m : -1 : 1
			A[j] = j * (A[j] - A[j + 1])
		end
	end
	return abs(A[1])
end

function quaseigual(v1,v2)
	erro = 0.001
	if (v1 - erro) < v2 < (v1 + erro)
		return true
	else
		return false
	end
end

function fatorial(n)
	fat = n
	while n > 1
		fat *= (n - 1)
		n -= 1
	end
	return fat
end

function sin(x)
	sinal = 1
	expoente = 3
	denominador = 3
	sin = x
	for iteracao in 1:10
		proxtermo = (x^expoente)/(fatorial(denominador))
		sin = sin + (-1)^sinal * proxtermo
		expoente += 2
		denominador += 2
		sinal += 1
	end
	return(sin)
end

function cos(x)
	sinal = 1
	expoente = 2
	denominador = 2
	cos = 1
	for iteracao in 1:10
		proxtermo = (x^expoente)/(fatorial(denominador))
		cos = cos + (-1)^sinal * proxtermo
		expoente += 2
		denominador += 2
		sinal += 1
	end
	return(cos)
end

function tan(x)
	tan = big(0)
	for iteracao in 1:10
		doisn = 2 * iteracao 
		proxtermo = 2^doisn * (2^doisn - 1) * bernoulli(iteracao) * x^(doisn - 1) * (1/fatorial(doisn))  
		tan += proxtermo
	end
	return(tan)
end

function check_sin(value, x)
	return quaseigual(value, sin(x))
end

function check_cos(value, x)
	return quaseigual(value, cos(x))
end

function check_tan(value, x)
	return quaseigual(value, tan(x))
end


function taylor_sin(x)
	sinal = 1
	expoente = 3
	denominador = 3
	sin = x
	for iteracao in 1:10
		proxtermo = (x^expoente)/(fatorial(denominador))
		sin = sin + (-1)^sinal * proxtermo
		expoente += 2
		denominador += 2
		sinal += 1
	end
	return(sin)
end

function taylor_cos(x)
	sinal = 1
	expoente = 2
	denominador = 2
	cos = 1
	for iteracao in 1:10
		proxtermo = (x^expoente)/(fatorial(denominador))
		cos = cos + (-1)^sinal * proxtermo
		expoente += 2
		denominador += 2
		sinal += 1
	end
	return(cos)
end

function taylor_tan(x)
	tan = big(0)
	for iteracao in 1:10
		doisn = 2 * iteracao 
		proxtermo = 2^doisn * (2^doisn - 1) * bernoulli(iteracao) * x^(doisn - 1) * (1/fatorial(doisn))  
		tan += proxtermo
	end
	return(tan)
end

function test()
	quaseigual(sin(0.5),0.479425) == true &&
 	quaseigual(sin(1),0.841470) == true &&
 	quaseigual(sin(-2),-0.90929) == true &&
 	quaseigual(sin(0),0) == true &&
 	quaseigual(cos(0.5),0.87758) == true && 
	quaseigual(cos(1),0.540302) == true &&
	quaseigual(cos(-2),-0.41614) == true &&
	quaseigual(cos(0),1) == true &&
	quaseigual(tan(0.5), 0.546302) == true &&
	quaseigual(tan(1),1.557407725) == true &&
	quaseigual(tan(-1),-1.55740772466) == true &&
	quaseigual(tan(-1.1),-1.96475965725) == true &&
	quaseigual(tan(0),0) == true &&
	check_sin(0.5, π/6) == true &&
	check_sin(1, π/2) == true &&
	check_sin(0, 0) == true &&
	check_sin(sqrt(3)/2, π/3) == true &&
	check_cos(sqrt(3)/2, π/6) == true &&
	check_cos(0, π/2) == true &&
	check_cos(1, 0) == true &&
	check_cos(1/2, π/3) == true &&
	check_tan(sqrt(3)/3, π/6) == true &&
	check_tan(0, 0) == true &&
	check_tan(sqrt(3), π/3) == true
	println("Tudo OK")
end

test()
